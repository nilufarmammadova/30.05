--Task 1
select employee_id, first_name, department_name, job_title, country_name
from employees e
inner join departments d on e.department_id = d.department_id
inner join jobs j on e.job_id = j.job_id
right join countries c on c.country_id = c.country_id;  

--Task 2
select employee_id, first_name, department_name, job_title, country_name
from employees e
inner join departments d on e.department_id = d.department_id
inner join countries c on c.country_id = c.country_id
left join jobs j on j.job_id = e.job_id;

--Task 3
select employee_id, first_name, department_name, job_title, country_name
from employees e
left join jobs j on e.job_id = j.job_id
left join countries c on c.country_id = c.country_id
inner join departments d on d.department_id = e.department_id;

--Task 4
select employee_id, first_name, department_name, job_title, country_name
from ((employees e inner join departments d on e.department_id = d.department_id)
inner join jobs j on e.job_id = e.job_id)
inner join countries c on c.country_id = c.country_id; 

--Task 5
select employee_id, first_name, department_name, job_title, country_name
from ((employees e left join departments d on e.department_id = d.department_id)
left join jobs j on e.job_id = e.job_id)
left join countries c on c.country_id = c.country_id; 
